package com.eve.chatsocket.utils;

import lombok.SneakyThrows;
import org.springframework.web.socket.WebSocketSession;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class WebSocketSessionManager {

    public static Map<String, WebSocketSession> SESSION_POOL = new ConcurrentHashMap<>();

    public static void add(String key, WebSocketSession session) {
        SESSION_POOL.put(key, session);
    }

    @SneakyThrows
    public static void removeAndClose(String key) {
        WebSocketSession session = SESSION_POOL.remove(key);
        if (session != null) {
            session.close();
        }
    }

    public static WebSocketSession getSession(String key) {
        return SESSION_POOL.get(key);
    }
}
