package com.eve.chatsocket.constn;

import com.google.gson.Gson;

public class ChatConst {

    public static String hostUrl = "http://spark-api.xf-yun.com/v1.1/chat";
    public static String APPID = "";//从开放平台控制台中获取
    public static String APIKEY = "";//从开放平台控制台中获取
    public static String APISecret = "";//从开放平台控制台中获取
    public static final Gson json = new Gson();
}
