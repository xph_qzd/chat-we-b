package com.eve.chatsocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ChatSocketApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChatSocketApplication.class, args);
    }

}
