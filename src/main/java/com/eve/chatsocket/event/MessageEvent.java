package com.eve.chatsocket.event;

import com.eve.chatsocket.entity.EventVo;
import org.springframework.context.ApplicationEvent;


public class MessageEvent extends ApplicationEvent {

    private EventVo eventVo;

    public MessageEvent(EventVo event) {
        super(event);
        this.eventVo = event;
    }

    public EventVo getEventVo() {
        return eventVo;
    }

    public void setEventVo(EventVo eventVo) {
        this.eventVo = eventVo;
    }

}
