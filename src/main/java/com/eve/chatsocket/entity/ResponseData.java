package com.eve.chatsocket.entity;

import com.google.gson.JsonObject;
import lombok.Data;

@Data

public class ResponseData {
    private JsonObject header;
    private  JsonObject payload;

    public JsonObject getHeader() {
        return header;
    }

    public JsonObject getPayload() {
        return payload;
    }
}
