package com.eve.chatsocket.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EventVo {
    private String sessionId;
    private Object message;
}
