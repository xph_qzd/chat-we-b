package com.eve.chatsocket.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebSocketController {
    /**
     * 测试
     * @return
     */
    @GetMapping("/test")
    public String test(){
        return "启动成功";
    }

}
